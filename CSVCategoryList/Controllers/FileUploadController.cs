﻿using CSVCategoryList.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;

namespace CSVCategoryList.Controllers
{
    public class FileUploadController : Controller
    {
        // GET: FileUpload
        public ActionResult UploadForm()
        {
            return View();
        }

        [HttpPost]
        public JsonResult UploadFile()
        {

            string Message, fileName, actualFileName;

            Message = fileName = actualFileName = string.Empty;

            bool flag = false;

            List<CategoryModel> categories = new List<CategoryModel>();
            List<CategoryTree> tree =new List<CategoryTree>();



            if (Request.Files != null)
            {
                var file = Request.Files[0];
                actualFileName = file.FileName;
                fileName = Guid.NewGuid() + Path.GetExtension(file.FileName);
                int size = file.ContentLength;

                try
                {
                    string path = Path.Combine(Server.MapPath("~/UploadedFiles"), fileName);
                    file.SaveAs(path);

                    string csvData = System.IO.File.ReadAllText(path);

                    //Execute a loop over the rows.
                    foreach (string row in csvData.Split('\n'))
                    {
                        if (!string.IsNullOrEmpty(row))
                        {
                            var csvRow = row.Split(',');

                            CategoryModel c = new CategoryModel();

                            c.ID = Int32.Parse(csvRow[0]);
                            c.Pid = csvRow[1] != "" ? (int?)Int32.Parse(csvRow[1]) : null;
                            c.Name = csvRow[2];

                            categories.Add(c);

                        }
                     
                    }
                    // Build category tree list 
                    tree = new CategoryTree().BuildTreeAndGetRoots(categories.OrderBy(c => c.Pid).ToList());
                   
                }
                catch (Exception)
                {
                    Message = "File upload failed! Please try again";
                }
               
            }
           
            return new JsonResult
            {
                Data = new
                {
                    Message = Message,
                    FileName = fileName,
                    Status = flag,
                    categories = JsonConvert.SerializeObject(tree,Formatting.None,
                    new JsonSerializerSettings() {
                        ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                    }) 
                }
            };

        
        }

    }
}