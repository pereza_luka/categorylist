﻿(function () {

    "use strict";
    angular.module('CsvApp', [])
        .controller('UploadController', function ($scope, FileUploadService) {
            // Variables
            $scope.Message = "";
            $scope.FileInvalidMessage = "";
            $scope.SelectedFileForUpload = null;
            $scope.IsFormSubmitted = false;
            $scope.IsFileValid = false;
            $scope.IsFormValid = false;
            $scope.showCategories = false;
            $scope.categories = {};
            $scope.uFileName = null;

            //Form Validation
            $scope.$watch("uploadForm.$valid", function (isValid) {
                $scope.IsFormValid = isValid;
            });

            //File Validation
            $scope.ChechFileValid = function (file) {
                var isValid = false;
                if ($scope.SelectedFileForUpload !== null) {

                    var type = file.name.split('.').pop()
                    if ((type === 'csv') && file.size <= (1024 * 1024)) {
                        $scope.FileInvalidMessage = "";
                        isValid = true;
                    }
                    else {
                        $scope.FileInvalidMessage = "Selected file is Invalid. (only file type csv and up to 1Mb size allowed)";
                    }
                }
                else {
                    $scope.FileInvalidMessage = "File required!";
                }
                $scope.IsFileValid = isValid;
            };

            //File Select event 
            $scope.selectFileforUpload = function (file) {
                $scope.SelectedFileForUpload = file[0];
            }


            //Save File
            $scope.SaveFile = function () {
                $scope.IsFormSubmitted = true;
                $scope.Message = "";
                $scope.ChechFileValid($scope.SelectedFileForUpload);
                if ($scope.IsFormValid && $scope.IsFileValid) {
                    FileUploadService.UploadFile($scope.SelectedFileForUpload).then(function (d) {
                        ClearForm();
                        $scope.uFileName = d.uFileName;
                        console.log(angular.fromJson(d.data.categories))
                        $scope.categories = angular.fromJson(d.data.categories)
                        $scope.showCategories = true;
                    }, function (e) {
                        alert(e);
                    });
                }

            };
            //Clear form 
            function ClearForm() {
                angular.forEach(angular.element("input[type='file']"), function (inputElem) {
                    angular.element(inputElem).val(null);
                });

                $scope.uploadForm.$setPristine();
                $scope.IsFormSubmitted = false;
            }

        })
        .factory('FileUploadService', function ($http, $q) {

            var fac = {};
            fac.UploadFile = function (file) {
                var formData = new FormData();
                formData.append("file", file);

                var defer = $q.defer();
                $http.post("/FileUpload/UploadFile", formData,
                    {
                        withCredentials: true,
                        headers: { 'Content-Type': undefined },
                        transformRequest: angular.identity
                    })
                    .then(function (d) {
                     
                        defer.resolve(d);
                       
                    }, function (error) {
                        defer.reject("File Upload Failed!");
                    })

                return defer.promise;
                    
                
            }
            return fac;

        });
})();