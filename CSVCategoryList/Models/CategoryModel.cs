﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace CSVCategoryList.Models
{
    public class CategoryModel
    {
        public int ID { get; set; }

        //Cat Name  
        public string Name { get; set; }

        //represnts Parent ID and it's nullable  
        public int? Pid { get; set; }

    }

    public class CategoryTree
    {
        public List<CategoryTree> Children = new List<CategoryTree>();
        public CategoryTree Parent { get; set; }
        public CategoryModel Data { get; set; }


        public List<CategoryTree> BuildTreeAndGetRoots(List<CategoryModel> actualObjects)
        {
            var lookup = new Dictionary<int, CategoryTree>();
            var rootNodes = new List<CategoryTree>();

            foreach (var item in actualObjects)
            {
                // add us to lookup
                CategoryTree ourNode;
                if (lookup.TryGetValue(item.ID, out ourNode))
                {   // was already found as a parent - register the actual object
                    ourNode.Data = item;
                }
                else
                {
                    ourNode = new CategoryTree() { Data = item };
                    lookup.Add(item.ID, ourNode);
                }

                
                if (item.Pid == null)
                {   // is a root node
                    rootNodes.Add(ourNode);
                }
                else
                {   // is a child row - so we have a parent
                    CategoryTree parentNode;
                    if (!lookup.TryGetValue((int)item.Pid, out parentNode))
                    {   // unknown parent, construct preliminary parent
                        parentNode = new CategoryTree();
                        lookup.Add((int)item.Pid, parentNode);
                    }

                    parentNode.Children.Add(ourNode);
                    ourNode.Parent = parentNode;
                }
            }

            return rootNodes;
        }
    }


}